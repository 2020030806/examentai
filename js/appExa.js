fetch("servicio.json")
      .then(response => response.json())
      .then(data => {
        // Obtener la tabla y crear una fila por cada elemento en el servicio web
        const table = document.getElementById("servicio");
        let totalGanancias = 0;

        data.forEach(item => {
          const row = document.createElement("tr");
          const ganancia = (item.preciovta * item.preciocompra) * item.cantidad;
          totalGanancias += ganancia;
          row.innerHTML = 
            "<td>" + item.idcliente + "</td>" +
            "<td>" + item.codigo + "</td>" +
            "<td>" + item.descripcion+ "</td>" +
            "<td>" + item.preciovta + "</td>" +
            "<td>" + item.preciocompra + "</td>" +
            "<td>" + item.cantidad + "</td>" +
            "<td>" + ganancia + "</td>"
            
          ;
          table.appendChild(row);

        });
        document.getElementById("totalGanancias").innerHTML = totalGanancias;
      });

     